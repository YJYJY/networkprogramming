//////////////////////////////////////////////////////////
// UDPClient文件


#include "../common/InitSock.h"
#include <stdio.h>
CInitSock initSock;		// 初始化Winsock库

int main()
{
	// 创建套节字
	SOCKET s = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (s == INVALID_SOCKET)
	{
		printf("Failed socket() %d \n", ::WSAGetLastError());
		return 0;
	}

	// 也可以在这里调用bind函数绑定一个本地地址
	// 否则系统将会自动安排

	// 填写远程地址信息
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(4567);
	// 注意，这里要填写服务器程序所在机器的IP地址
	// 如果你的计算机没有联网，直接使用127.0.0.1即可
	addr.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");

	// 获取socket 的类型
	int sockettype = 0;
	int typesize = sizeof(sockettype);
	if (::getsockopt(s,SOL_SOCKET,SO_TYPE,(char*)&sockettype,(int*)&typesize)<0)
	{
		perror("getsockopt: ");
	}
	printf("the socket type is %d \n", sockettype);


	//获取发送和接收缓冲区，默认大小 8192
	int  cur_size = 0;
	int size_len = sizeof(cur_size);

	if (::getsockopt(s, SOL_SOCKET, SO_RCVBUF, (char*)&cur_size, (int*)&size_len)<0)
	{
		perror("getsockopt: ");
	}
	printf("the rcvbuf len is %d \n", cur_size);

	if (::getsockopt(s, SOL_SOCKET, SO_SNDBUF, (char*)&cur_size, (int*)&size_len) < 0)
	{
		perror("getsockopt: ");
	}
	printf("the sndbuf len is %d \n", cur_size);


	// 发送数据, 一次性调用 sendto 接口只能发送60k 左右的数据，否则会提示发送失败
	int nlen = 60 * 1024;
	char* text = (char*)malloc(nlen);
	memset(text, 1, nlen);

	nlen = ::sendto(s, text, nlen, 0, (sockaddr*)&addr, sizeof(addr));
	if (nlen < 0 )
	{
		printf("send message error %d \n", ::WSAGetLastError());
	}

	::closesocket(s);
	getchar();
	return 0;
}